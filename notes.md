This gist is to provide the resultant code of [a BookStack video guide](https://youtu.be/5bZ7zlNEphc).
The code existed at `themes/custom/layouts/parts/export-body-start.blade.php` relative to the BookStack installation folder.

This uses the [visual theme system](https://github.com/BookStackApp/BookStack/blob/development/dev/docs/visual-theme-system.md) so you need to have an theme folder active in your instance.
The code assumes you have a `logo.png` file in the root of your custom theme folder.